import 'package:flutter/material.dart';

//Lab 3 - Chips, buttons and card, EC-Dialog boxes or languages
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Covid-19 Symptom Checker',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      debugShowCheckedModeBanner: false,
      home: UserInfo(),
    );
  }
}

//from copy Esai
class UserInfo extends StatefulWidget {
  UserInfo({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _UserInfoState();
}

var genderPass = 'None';
var agePass = 0;

class _UserInfoState extends State<UserInfo> {
  static String gender = ''; //card with chips inside
   int age = 1; //dropdown
  static bool overweight, diabetes, respDisease, heartDisease; //filter chips
  static List choiceGender = ['Male', 'Female'];
  int value; //for gender selection

  @override
  void initState() {
    super.initState();
    agePass = 0;
  }

  @override
  Widget build(BuildContext context) {
    //implement bottom navigation
    return Scaffold(
      backgroundColor: new Color(0xffb2e8ff),
      appBar: AppBar(
        backgroundColor: new Color(0xffb2e8ff),
        leading: IconButton(
          icon: Icon(Icons.assignment, color: Colors.red),
          onPressed: () {
            showAboutDialog(
                context: context,
                applicationIcon:
                    Icon(Icons.ac_unit, size: 60, color: new Color(0xffb2e8ff)),
                applicationName: 'Covid-19 Symptom Checker',
                applicationVersion: '1.0.0',
                applicationLegalese: 'Developed by Esai, Tyler, Zsuzsanna. This is not a substitute for professional medical advice, as the developers are computer science students.');
          },
        ),
        title: Text('Covid-19 Symptom Checker',
            style: TextStyle(
              color: Colors.red,
            )),
        actions: <Widget>[],
      ),
      body: Center(
        child: Column(children: [
          SizedBox(height: 10),
          Container(
            padding: EdgeInsets.all(1.0),
            child: Image(
              image: AssetImage('assets/virus390x75.jpg'),
            ),
          ),
          SizedBox(height: 10),
          //Gender
          Padding(
            padding: const EdgeInsets.only(right: 5.0, left: 5.0, bottom: 5),
            child: Card(
              color: new Color(0xffe0f5ff),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [

                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('Sex',
                          style: TextStyle(fontSize: 20, color: Colors.red))
                    ],
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: List<Widget>.generate(2, (int index) {
                                return FilterChip(
                                  label: Text(
                                    choiceGender.elementAt(index),
                                    style: TextStyle(fontSize: 20),
                                  ),
                                  backgroundColor: Colors.blue[200],
                                  selectedColor: Colors.red[200],
                                  selected: value == index,
                                  onSelected: (bool selected) {
                                    setState(() {
                                      value = selected ? index : null;
                                      if (selected) {
                                        gender = 'Male';
                                      } else if (!selected) {
                                        gender = 'Female';
                                      } else {
                                        gender = '';
                                      }
                                      genderPass =
                                          choiceGender.elementAt(index);
                                      print(choiceGender.elementAt(index) +
                                          ' has been selected.');
                                    });
                                  },
                                );
                              }).toList(),
                            ),
                          ),
                        ),
                      ] //inner children
                      ),
                  SizedBox(
                    height: 8,
                  ),
                ],
              ),
            ),
          ),

          //Age dropdown
          Padding(
            padding: const EdgeInsets.only(right: 5.0, left: 5),
            child: Card(
              color: new Color(0xffe0f5ff),
              child: Column(
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('Age',
                          style: TextStyle(fontSize: 20, color: Colors.red))
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        width: 300,
                        height: 100,
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              DropdownButton<int>(
                                value: age,
                                icon: Icon(Icons.arrow_downward,
                                    color: Colors.blueAccent),
                                iconSize: 24,
                                elevation: 20,
                                style: TextStyle(color: Colors.blue),
                                underline: Container(
                                  height: 2,
                                  color: Colors.blueAccent,
                                ),
                                onChanged: (int newValue) {
                                  setState(() {
                                    age = newValue;
                                    print('Age: ' + age.toString());
                                    agePass = age;
                                  });
                                },
                                items: <int>[
                                  1,
                                  2,
                                  3,
                                  4,
                                  5,
                                  6,
                                  7,
                                  8,
                                  9,
                                  10,
                                  11,
                                  12,
                                  13,
                                  14,
                                  15,
                                  16,
                                  17,
                                  18,
                                  19,
                                  20,
                                  21,
                                  22,
                                  23,
                                  24,
                                  25,
                                  26,
                                  27,
                                  28,
                                  29,
                                  30,
                                  31,
                                  32,
                                  33,
                                  34,
                                  35,
                                  36,
                                  37,
                                  38,
                                  39,
                                  40,
                                  41,
                                  42,
                                  43,
                                  44,
                                  45,
                                  46,
                                  47,
                                  48,
                                  49,
                                  50,
                                  51,
                                  52,
                                  53,
                                  54,
                                  55,
                                  56,
                                  57,
                                  58,
                                  59,
                                  60,
                                  61,
                                  62,
                                  63,
                                  64,
                                  65,
                                  66,
                                  67,
                                  68,
                                  69,
                                  70,
                                  71,
                                  72,
                                  73,
                                  74,
                                  75,
                                  76,
                                  77,
                                  78,
                                  79,
                                  80,
                                  81,
                                  82,
                                  83,
                                  84,
                                  85,
                                  86,
                                  87,
                                  88,
                                  89,
                                  90,
                                  91,
                                  92,
                                  93,
                                  94,
                                  95,
                                  96,
                                  97,
                                  98,
                                  99,
                                  100
                                ].map<DropdownMenuItem<int>>((int value) {
                                  return DropdownMenuItem<int>(
                                    value: value,
                                    child: Text(value.toString()),
                                  );
                                }).toList(),
                              ),
                            ]),
                      ),
                    ], //inner children
                  ),
                ],
              ),
            ),
          ),

          //Co-morbidities
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: Card(
              color: Color(0xffe0f5ff),
              child: Column(
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('Co-morbidities',
                          style: TextStyle(fontSize: 20, color: Colors.red))
                    ],
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        width: 350,
                        height: 110,
                        child: ListView(
                          children: [
                            Wrap(
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    CoMordbidWidget(name: 'Overweight'),
                                    CoMordbidWidget(name: 'Diabetes'),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    CoMordbidWidget(name: 'Resp. disease'),
                                    CoMordbidWidget(name: 'Heart disease'),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),

          SizedBox(height: 5),
          //Go to next page
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            RaisedButton(
              child: Text('Enter Symptoms', style: TextStyle(fontSize: 20)),
              color: Color(0xffe0f5ff),
              onPressed: () {
                createCoMorbs();
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => SymptomScreen()));
              },
            ),
          ]),
        ]),
      ),
    );
  }

  createCoMorbs() {
    CoMorbsTable table = new CoMorbsTable();

    for (int i = 0; i < ComorbidList.length; i++) {
      if (i == 0) table.first = ComorbidList[i];
      if (i == 1) table.second = ComorbidList[i];
      if (i == 2) table.third = ComorbidList[i];
      if (i == 3) table.fourth = ComorbidList[i];
    }

    tablePass = table;
    print("Printing");
    print(tablePass);
  }
}

CoMorbsTable tablePass;

class CoMordbidWidget extends StatefulWidget {
  final String name;

  CoMordbidWidget({Key key, this.name}) : super(key: key);

  @override
  _CoMordbidWidgetState createState() => _CoMordbidWidgetState();
}

List ComorbidList = [];

class CoMorbsTable {
  String first;
  String second;
  String third;
  String fourth;

  CoMorbsTable() {
    first = "";
    second = "";
    third = " ";
    fourth = "";
  }
}

class SymptomsTable {
  String first;
  String second;
  String third;
  String fourth;
  String fifth;

  SymptomsTable() {
    first = "";
    second = " ";
    third = " ";
    fourth = " ";
    fifth = " ";
  }
}

class _CoMordbidWidgetState extends State<CoMordbidWidget> {
  bool _isSelect = false;

  @override
  Widget build(BuildContext context) {
    return FilterChip(
      label: Text(
        widget.name,
        style: TextStyle(fontSize: 18),
      ),
      backgroundColor: Colors.blue[200],
      selectedColor: Colors.red[200],
      selected: _isSelect,
      onSelected: (isSelected) {
        setState(() {
          _isSelect = isSelected;
          if (widget.name == 'Overweight') {
            _UserInfoState.overweight = _isSelect;
            if (_isSelect)
              ComorbidList.add('Overweight');
            else
              ComorbidList.remove('Overweight');
          }
          if (widget.name == 'Diabetes') {
            _UserInfoState.diabetes = _isSelect;
            if (_isSelect)
              ComorbidList.add('Diabetes');
            else
              ComorbidList.remove('Diabetes');
          }
          if (widget.name == 'Resp. disease') {
            _UserInfoState.respDisease = _isSelect;
            if (_isSelect)
              ComorbidList.add('Resp disease');
            else
              ComorbidList.remove('Resp disease');
          }
          if (widget.name == 'Heart disease') {
            _UserInfoState.heartDisease = _isSelect;
            if (_isSelect)
              ComorbidList.add('Heart disease');
            else
              ComorbidList.remove('Heart disease');
          }
          print(
              //prints current selections
              'Overweight: ' +
                  _UserInfoState.overweight.toString() +
                  ' | ' +
                  ' Diabetes: ' +
                  _UserInfoState.diabetes.toString() +
                  ' | ' +
                  ' Resp. disease: ' +
                  _UserInfoState.respDisease.toString() +
                  ' | ' +
                  ' Heart disease: ' +
                  _UserInfoState.heartDisease.toString());

          print("List: ");
          print(ComorbidList);
        });
      },
    );
  }
}

//from copy
//Tyler
List SymptomsList = [];

class SymptomScreen extends StatefulWidget {
  @override
  _SymptomScreenState createState() => _SymptomScreenState();
}

class _SymptomScreenState extends State<SymptomScreen> {
  bool _isSelected = false; // Fever
  bool _isSelected2 = false; // Dry Coughing
  bool _isSelected3 = false; // Fatigue
  bool _isSelected4 = false; // Difficulty Breathing
  bool _isSelected5 = false; // Body aches
  bool _isSelected6 = false; // Headache
  bool _isSelected7 = false; // Diarrhea
  bool _isSelected8 = false; // Nausea
  bool _isSelected9 = false; // Anosmia
  bool _isSelected10 = false; // Chills
  bool _isSelected11 = false; // Blue lips
  bool _isSelected12 = false; // Sore throat

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: new Color(0xffb2e8ff),
      appBar: AppBar(
        backgroundColor: new Color(0xffb2e8ff),
        centerTitle: true,
        title: Text(
          'Symptoms',
          style: TextStyle(color: Colors.red),
        ),
        actions: <Widget>[],
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(height: 20),
          SizedBox(
            height: 20,
            child: Text(
              'Select all that apply:',
              style: TextStyle(fontSize: 18, color: Colors.red),
            ),
          ),
          SizedBox(height: 20),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 60),
                      child: InputChip(
                        avatar: CircleAvatar(
                            //child: Text("Fe"),
                            child: Icon(Icons.ac_unit)),
                        label: Text("Fever"),
                        backgroundColor: new Color(0xffe0f5ff),
                        selected: _isSelected,
                        selectedColor: Colors.red[200],
                        onSelected: (bool selected) {
                          setState(() {
                            _isSelected = selected;
                          });
                        },
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 60),
                      child: InputChip(
                        avatar: CircleAvatar(
                          child: Icon(Icons.account_circle),
                        ),
                        label: Text("Coughing"),
                        backgroundColor: new Color(0xffe0f5ff),
                        selected: _isSelected2,
                        selectedColor: Colors.red[200],
                        onSelected: (bool selected) {
                          setState(() {
                            _isSelected2 = selected;
                          });
                        },
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 60),
                      child: InputChip(
                        avatar: CircleAvatar(
                            child: Icon(Icons.airline_seat_flat_angled)),
                        label: Text("Fatigue"),
                        backgroundColor: new Color(0xffe0f5ff),
                        selected: _isSelected3,
                        selectedColor: Colors.red[200],
                        onSelected: (bool selected) {
                          setState(() {
                            _isSelected3 = selected;
                          });
                        },
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 60),
                      child: InputChip(
                        avatar: CircleAvatar(
                          child: Icon(Icons.all_out),
                        ),
                        label: Text("Difficulty Breathing"),
                        backgroundColor: new Color(0xffe0f5ff),
                        selected: _isSelected4,
                        selectedColor: Colors.red[200],
                        onSelected: (bool selected) {
                          setState(() {
                            _isSelected4 = selected;
                          });
                        },
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 60),
                      child: InputChip(
                        avatar: CircleAvatar(
                          child: Icon(Icons.airline_seat_recline_extra),
                        ),
                        label: Text("Body aches"),
                        backgroundColor: new Color(0xffe0f5ff),
                        selected: _isSelected5,
                        selectedColor: Colors.red[200],
                        onSelected: (bool selected) {
                          setState(() {
                            _isSelected5 = selected;
                          });
                        },
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 60),
                      child: InputChip(
                        avatar: CircleAvatar(
                          child: Icon(Icons.backup),
                        ),
                        label: Text("Headache"),
                        backgroundColor: new Color(0xffe0f5ff),
                        selected: _isSelected6,
                        selectedColor: Colors.red[200],
                        onSelected: (bool selected) {
                          setState(() {
                            _isSelected6 = selected;
                          });
                        },
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 60),
                      child: InputChip(
                        avatar: CircleAvatar(
                          child: Icon(Icons.border_all),
                        ),
                        label: Text("Diarrhea"),
                        backgroundColor: new Color(0xffe0f5ff),
                        selected: _isSelected7,
                        selectedColor: Colors.red[200],
                        onSelected: (bool selected) {
                          setState(() {
                            _isSelected7 = selected;
                          });
                        },
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 60),
                      child: InputChip(
                        avatar: CircleAvatar(
                          child: Icon(Icons.toys),
                        ),
                        label: Text("Nausea"),
                        backgroundColor: new Color(0xffe0f5ff),
                        selected: _isSelected8,
                        selectedColor: Colors.red[200],
                        onSelected: (bool selected) {
                          setState(() {
                            _isSelected8 = selected;
                          });
                        },
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 60),
                      child: InputChip(
                        avatar: CircleAvatar(
                          child: Icon(Icons.brightness_auto),
                        ),
                        label: Text("Anosmia"),
                        backgroundColor: new Color(0xffe0f5ff),
                        selected: _isSelected9,
                        selectedColor: Colors.red[200],
                        onSelected: (bool selected) {
                          setState(() {
                            _isSelected9 = selected;
                          });
                        },
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 60),
                      child: InputChip(
                        avatar: CircleAvatar(
                          child: Icon(Icons.camera),
                        ),
                        label: Text("Chills"),
                        backgroundColor: new Color(0xffe0f5ff),
                        selected: _isSelected10,
                        selectedColor: Colors.red[200],
                        onSelected: (bool selected) {
                          setState(() {
                            _isSelected10 = selected;
                          });
                        },
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 60),
                      child: InputChip(
                        avatar: CircleAvatar(
                          child: Icon(Icons.child_care),
                        ),
                        label: Text("Blue lips"),
                        backgroundColor: new Color(0xffe0f5ff),
                        selected: _isSelected11,
                        selectedColor: Colors.red[200],
                        onSelected: (bool selected) {
                          setState(() {
                            _isSelected11 = selected;
                          });
                        },
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 60),
                      child: InputChip(
                        avatar: CircleAvatar(
                          child: Icon(Icons.extension),
                        ),
                        label: Text("Sore Throat"),
                        backgroundColor: new Color(0xffe0f5ff),
                        selected: _isSelected12,
                        selectedColor: Colors.red[200],
                        onSelected: (bool selected) {
                          setState(() {
                            _isSelected12 = selected;
                          });
                        },
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 5,
                ),
              ],
            ),
          ),
          //SizedBox(height: 20),
          Column(
            children: [
              FloatingActionButton(
                backgroundColor: new Color(0xffe0f5ff),
                child: Icon(
                  Icons.check,
                  color: Colors.red,
                ),

                //label: Text('Check Results', style: TextStyle(fontSize: 20)),
                onPressed: () {
                  SymptomsList.clear();
                  if (_isSelected == true) {
                    SymptomsList.add("Fever");
                  }
                  if (_isSelected2 == true) {
                    SymptomsList.add("Dry Cough");
                  }

                  if (_isSelected3 == true) {
                    SymptomsList.add("Fatigue");
                  }

                  if (_isSelected4 == true) {
                    SymptomsList.add("Difficulty Breathing");
                  }

                  if (_isSelected5 == true) {
                    SymptomsList.add("Body aches");
                  }

                  if (_isSelected6 == true) {
                    SymptomsList.add("Headache");
                  }

                  if (_isSelected7 == true) {
                    SymptomsList.add("Diarrhea");
                  }

                  if (_isSelected8 == true) {
                    SymptomsList.add("Nausea");
                  }
                  if (_isSelected9 == true) {
                    SymptomsList.add("Anosmia");
                  }
                  if (_isSelected10 == true) {
                    SymptomsList.add("Chills");
                  }
                  if (_isSelected11 == true) {
                    SymptomsList.add("Blue lips");
                  }
                  if (_isSelected12 == true) {
                    SymptomsList.add("Sore throat");
                  }

                  print("Symptom List from Tyler button");
                  print(SymptomsList);
                  createSymptoms();
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Results()));
                },
              ),
            ],
          ),
          SizedBox(height: 30),
        ],
      ),
    );
  }

  createSymptoms() {
    List<SymptomsTable> list = [];
    SymptomsTable table = new SymptomsTable();
    SymptomsTable table2 = new SymptomsTable();
    SymptomsTable table3 = new SymptomsTable();
    var lengthList = SymptomsList.length-1;
    var i = 0; //position in list

    var run1 = false;
    var run2 = false;
    var run3 = false;

    if(lengthList <= 0) {}
    else if(lengthList < 5) run1 = true;
    else if(lengthList < 10) {
      run1 = true; run2 = true;
    }
    else {
      run1 = true; run2 = true; run3 = true;
    }

    while(i < 5 && run1) {
      print("list1:" + i.toString() + " run1 is: " + run1.toString() + " length: " + lengthList.toString());
        if(i == 0) {
          table.first = SymptomsList[i];
          print("added: " + table.first);
          i++;
          if (i > lengthList) break;
        }
        if (i == 1) {
          table.second = SymptomsList[i];
          i++;
          if (i > lengthList) break;
        }
        if (i == 2) {
          table.third = SymptomsList[i];
          i++;
          if (i > lengthList) break;
        }
        if (i == 3) {
          table.fourth = SymptomsList[i];
          i++;
          if (i > lengthList) break;
        }
        if(i == 4) {
          table.fifth = SymptomsList[i];
          i++;
          if (i > lengthList) break;
        }

    }
    print('i is: ' + i.toString() + " runs2 is: " + run2.toString());
    while(i < 10 && run2) {
      if(i == 5) {
        table2.first = SymptomsList[i];
        print("added: " + table2.first);
        i++;
        if (i > lengthList) break;
      }
      if (i == 6) {
        table2.second = SymptomsList[i];
        i++;
        if (i > lengthList) break;
      }
      if (i == 7) {
        table2.third = SymptomsList[i];
        i++;
        if (i > lengthList) break;
      }
      if (i == 8) {
        table2.fourth = SymptomsList[i];
        i++;
        if (i > lengthList) break;
      }
      if(i == 9) {
        table2.fifth = SymptomsList[i];
        i++;
        if (i > lengthList) break;
      }

    }

    while(i < 12 && run3) {
      if(i == 10) {
        table3.first = SymptomsList[i];
        i++;
        if (i > lengthList) break;
      }
      if (i == 11) {
        table3.second = SymptomsList[i];
        i++;
        if (i > lengthList) break;
      }
      if (i == 12) {
        table3.third = SymptomsList[i];
        i++;
        if (i > lengthList) break;
      }


    }


    list.add(table);
    if(SymptomsList.length > 5) list.add(table2);
    if(SymptomsList.length > 10) list.add(table3);

    symptomsPass = list;
  }
}

var symptomsPass;

//Zsuzsanna
class Results extends StatelessWidget {
  var _symptomsTotal = SymptomsList.length;
  var _comorbiditiesTotal = ComorbidList.length;
  var _age = agePass;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: new Color(0xffb2e8ff),
      appBar: AppBar(
        backgroundColor: new Color(0xffb2e8ff),
        centerTitle: true,
        title: Text(
          'Results',
          style: TextStyle(color: Colors.red),
        ),
        leading: IconButton(
          icon: Icon(Icons.repeat, color: Colors.red),
          onPressed: () {
            showAlertDialog(context);
          },
        ),
        // title: Text('Results'),
        //actions: <Widget>[],
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.only(left: 5, right: 5, top: 10),
                child: Card(
                  color: new Color(0xffe0f5ff),
                  child: cardToUse(_symptomsTotal, _comorbiditiesTotal),
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 5, right: 5),
                child: Card(
                  color: new Color(0xffe0f5ff),
                  child: ageCardToUse(_age),
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                  top: 10,
                ),
                // child: Text('Results', style: TextStyle(fontSize: 30,color: Colors.deepOrange, ),),
              ),
              Container(
                padding: EdgeInsets.only(left: 5, right: 5),
                child: Card(color: new Color(0xffe0f5ff), child: InputTable()),
              ),
              Container(
                width: 410,
                padding: EdgeInsets.only(left: 5, right: 5),
                child: Card(
                    color: new Color(0xffe0f5ff),
                    child: (ComorbidList.length < 1)
                        ? ComorbidTableNone()
                        : ComorbiditiesTable()),
              ),
              Container(
                width: 410,
                padding: EdgeInsets.only(left: 5, right: 5),
                child: Card(
                    color: new Color(0xffe0f5ff),
                    child: (SymptomsList.length == 0)
                        ? SymptomsTableNone()
                        : SymptomsTableScreen()),
              ),
              RaisedButton.icon(
                color: new Color(0xffe0f5ff),
                icon: Icon(Icons.repeat),
                elevation: 10,
                hoverColor: Colors.deepOrange,
                label: Text(
                  'Retry',
                  style: TextStyle(fontSize: 20),
                ),
                onPressed: () {
                  showAlertDialog(context);
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> showAlertDialog(BuildContext context) async {
    return showDialog(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return AlertDialog(
            //idea and some code from StackOverflow
            content: new Container(
              width: 260.0,
              height: 275.0,
              decoration: new BoxDecoration(
                shape: BoxShape.rectangle,
                color: const Color(0xFFFFFF),
                borderRadius: new BorderRadius.all(new Radius.circular(32.0)),
              ),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  // dialog top
                  new Expanded(
                    child: new Row(
                      children: <Widget>[
                        Center(
                          child: new Container(
                            // padding: new EdgeInsets.all(10.0),
                            decoration: new BoxDecoration(
                              color: Colors.white,
                            ),
                            child: Text(
                              'You Are About to Erase All Input!',
                              style: TextStyle(
                                color: Colors.red,
                                fontSize: 18.0,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),

                  // dialog center
                  new Expanded(
                    child: new Container(
                        child: new Text(
                      'Are you sure you want to reset?',
                      style: TextStyle(fontSize: 20),
                    )),
                    flex: 1,
                  ),

                  // dialog bottom
                  new Expanded(
                    child: new Container(
                      padding: new EdgeInsets.all(8.0),
                      decoration: new BoxDecoration(
                        color: const Color(0xffe0f5ff),
                      ),
                      child: FlatButton(
                        child: Text(
                          'YES, RESET',
                          style: TextStyle(color: Colors.black, fontSize: 18),
                        ),
                        onPressed: () {
                          ComorbidList = [];
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => UserInfo()));
                        },
                      ),
                    ),
                  ),

                  SizedBox(
                    height: 10,
                  ),
                  // dialog bottom
                  new Expanded(
                    child: new Container(
                      padding: new EdgeInsets.all(8.0),
                      decoration: new BoxDecoration(
                        color: const Color(0xffe0f5ff),
                      ),
                      child: FlatButton(
                        child: Text(
                          'NO, BACK TO RESULTS',
                          style: TextStyle(color: Colors.black, fontSize: 18),
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  Widget ageCardToUse(int age) {
    if (age == 0)
      return NoAgeSelected();
    else if (age < 60)
      return SafeAge();
    else
      return DangerAge();
  }

  Widget cardToUse(int symp, int comorbs) {
    if (symp == 1 && comorbs == 0)
      return YellowCardTwo();
    else if (symp == 0 && comorbs >= 1)
      return YellowCardOne();
    else if (symp >= 2 && comorbs == 0)
      return RedCardOne();
    else if (symp >= 1 && comorbs >= 1)
      return RedCardTwo();
    else
      return GreenCard();
  }

  Widget tableToUse(int count) {
    if (count == 0)
      return SymptomsTableNone();
    else
      return SymptomsTableScreen();
  }
}

class SafeAge extends StatelessWidget {
  SafeAge({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.green[300],
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            contentPadding: EdgeInsets.all(10),
            leading: Icon(
              Icons.cake,
              size: 40,
            ),
            title: Text(
              'Safe Age',
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
            subtitle: Text(
              'Your age is in the population that is considered to be safer.',
              style: TextStyle(fontSize: 15),
            ),
          ),
        ],
      ),
    );
  }
}

class NoAgeSelected extends StatelessWidget {
  NoAgeSelected({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.blue[200],
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            contentPadding: EdgeInsets.all(10),
            leading: Icon(
              Icons.help_outline,
              size: 40,
            ),
            title: Text(
              'No Age Selected',
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
            subtitle: Text(
              'Your age is an important indicator of disease severity. Please Retry.',
              style: TextStyle(fontSize: 15),
            ),
          ),
        ],
      ),
    );
  }
}

class DangerAge extends StatelessWidget {
  DangerAge({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.red[300],
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            contentPadding: EdgeInsets.all(10),
            leading: Icon(
              Icons.add_alert,
              size: 40,
            ),
            title: Text(
              'Cautious Age',
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
            subtitle: Text(
              'Your age is in the section of the population that needs to practice safer protocol.',
              style: TextStyle(fontSize: 15),
            ),
          ),
        ],
      ),
    );
  }
}

//1+ symp; 1+ comorb
class RedCardOne extends StatelessWidget {
  RedCardOne({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.red[300],
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            contentPadding: EdgeInsets.all(10),
            leading: Icon(
              Icons.bug_report,
              size: 40,
            ),
            title: Text(
              'Seek Medical Advice',
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
            subtitle: Text(
              'You are showing at least one symptom. Seek medical advice as soon as possible.',
              style: TextStyle(fontSize: 15),
            ),
          ),
        ],
      ),
    );
  }
}

//1+ symp, 1+ comorb
class RedCardTwo extends StatelessWidget {
  RedCardTwo({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.red[300],
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            contentPadding: EdgeInsets.all(10),
            leading: Icon(
              Icons.bug_report,
              size: 40,
            ),
            title: Text(
              'Seek Medical Advice',
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
            subtitle: Text(
              'You are showing symptoms and have at least one co-morbidity. Seek medical advice as soon as possible.',
              style: TextStyle(fontSize: 15),
            ),
          ),
        ],
      ),
    );
  }
}

//0 symp; 0 comorb
class GreenCard extends StatelessWidget {
  GreenCard({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.green[300],
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            contentPadding: EdgeInsets.all(10),
            leading: Icon(
              Icons.check,
              size: 40,
            ),
            title: Text(
              'No Signs of Infection',
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
            subtitle: Text(
              'You are showing no symptoms. Keep practicing all safety protocol.',
              style: TextStyle(fontSize: 15),
            ),
          ),
        ],
      ),
    );
  }
}

//no sympt; 1 comorb
class YellowCardOne extends StatelessWidget {
  YellowCardOne({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.yellow[300],
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            contentPadding: EdgeInsets.all(10),
            leading: Icon(
              Icons.help_outline,
              size: 40,
            ),
            title: Text(
              'Caution Advised',
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
            subtitle: Text(
              'You are showing no symptoms, but have at least one co-morbidity. Keep practicing all safety protocol.',
              style: TextStyle(fontSize: 15),
            ),
          ),
        ],
      ),
    );
  }
}

//1 sympt; no comorb
class YellowCardTwo extends StatelessWidget {
  YellowCardTwo({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.yellow[300],
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const ListTile(
            contentPadding: EdgeInsets.all(10),
            leading: Icon(
              Icons.help_outline,
              size: 40,
            ),
            title: Text(
              'Caution Advised',
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
            subtitle: Text(
              'You are showing a symptom. It is not possible to determine an infection. Keep practicing all safety protocol.',
              style: TextStyle(fontSize: 15),
            ),
          ),
        ],
      ),
    );
  }
}


class InputTable extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var gender = genderPass;

    return DataTable(
      columnSpacing: 15,
      columns: const <DataColumn>[
        DataColumn(
          label: Text(
            'Input',
            style: TextStyle(
              fontSize: 20,
              color: Colors.red,
            ),
          ),
        ),
        DataColumn(
          label: Text(
            ' ',
          ),
        ),
        DataColumn(
          label: Text(
            '',
            style: TextStyle(fontSize: 20, fontStyle: FontStyle.italic),
          ),
        ),
        DataColumn(
          label: Text(
            '',
            style: TextStyle(fontSize: 20, fontStyle: FontStyle.italic),
          ),
        ),
        DataColumn(
          label: Text(
            '',
            style: TextStyle(fontStyle: FontStyle.italic),
          ),
        ),
      ],
      rows: <DataRow>[
        DataRow(
          cells: <DataCell>[
            DataCell(Text(
              'Gender',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            )),
            DataCell(Text(gender, style: TextStyle(fontSize: 15))),
            DataCell(Text('                 ')),
            DataCell(Text('                 ')),
            DataCell(Text('                 ')),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(Text(
              'Age',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            )),
            DataCell(Text(agePass.toString(), style: TextStyle(fontSize: 15))),
            DataCell(Text('                  ')),
            DataCell(Text('                  ')),
            DataCell(Text('                  ')),
          ],
        ),
      ],
    );
  }
}


class ComorbidTableNone extends StatelessWidget {
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Container(
          padding: EdgeInsets.all(10),
          child: Text(
            'No Comorbidities',
            style: TextStyle(
              fontSize: 20,
              color: Colors.red,
            ),
          ),
        ),
      ],
    );
  }
}

class SymptomsTableNone extends StatelessWidget {
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Container(
          padding: EdgeInsets.all(10),
          child: Text(
            'No Symptoms',
            style: TextStyle(
              fontSize: 20,
              color: Colors.red,
            ),
          ),
        ),
      ],
    );
  }
}

class ComorbiditiesTable extends StatefulWidget {
  @override
  ComorbiditiesTableState createState() {
    return new ComorbiditiesTableState();
  }
}

class ComorbiditiesTableState extends State<ComorbiditiesTable> {
  @override
  Widget build(BuildContext context) {
    List<CoMorbsTable> list = List<CoMorbsTable>();
    list.add(tablePass);

    print("morbid list length: " + ComorbidList.length.toString());

    return DataTable(
        columnSpacing: 15,
        columns: const <DataColumn>[
          DataColumn(
            label: Text(
              'Comorbidities',
              style: TextStyle(
                fontSize: 16,
                color: Colors.red,
              ),
            ),
          ),
          DataColumn(
            label: Text(
              ' ',
            ),
          ),
          DataColumn(
            label: Text(
              '',
              style: TextStyle(fontSize: 20, fontStyle: FontStyle.italic),
            ),
          ),
          DataColumn(
            label: Text(
              '',
              style: TextStyle(fontSize: 20, fontStyle: FontStyle.italic),
            ),
          ),
          DataColumn(
            label: Text(
              '',
              style: TextStyle(fontStyle: FontStyle.italic),
            ),
          ),
        ],
        rows: list
            .map((item) => DataRow(cells: [
                  DataCell(Text(item.first)),
                  DataCell(Text(item.second)),
                  DataCell(Text(item.third)),
                  DataCell(Text(item.fourth)),
                  DataCell(Text('')),
                ]))
            .toList());
  }
}

class SymptomsTableScreen extends StatefulWidget {
  @override
  SymptomsTableState createState() {
    return new SymptomsTableState();
  }
}

class SymptomsTableState extends State<SymptomsTableScreen> {
  @override
  Widget build(BuildContext context) {
    List<SymptomsTable> list = symptomsPass;

    print("morbid list length: " + ComorbidList.length.toString());

    return DataTable(
        columnSpacing: 15,
        columns: const <DataColumn>[
          DataColumn(
            label: Text(
              'Symptoms',
              style: TextStyle(
                fontSize: 18,
                color: Colors.red,
              ),
            ),
          ),
          DataColumn(
            label: Text(
              ' ',
            ),
          ),
          DataColumn(
            label: Text(
              '',
              style: TextStyle(fontSize: 20, fontStyle: FontStyle.italic),
            ),
          ),
          DataColumn(
            label: Text(
              '',
              style: TextStyle(fontSize: 20, fontStyle: FontStyle.italic),
            ),
          ),
          DataColumn(
            label: Text(
              '',
              style: TextStyle(fontStyle: FontStyle.italic),
            ),
          ),
        ],
        rows: list
            .map((item) => DataRow(cells: [
                  DataCell(Text(item.first)),
                  DataCell(Text(item.second)),
                  DataCell(Text(item.third)),
                  DataCell(Text(item.fourth)),
                  DataCell(Text(item.fifth)),
                ]))
            .toList());
  }
}
